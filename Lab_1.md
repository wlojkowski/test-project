**Zadanie 1.** (0.3 pkt) Stwórz projekt Reacta za pomocą polecenia npx create-react-app. Będziemy tworzyć prostą aplikację do obsługi listy To-do. 

Stwórz formularz, który będzie zawierał dwa pola:
- text - opisujące nasze to-do
- date - wyznaczające termin zadania

Obok formularza dodaj przycisk, który będzie zatwierdzał dodanie nowego elementu do listy to-do. 

**Zadanie 2.** (0.2 pkt) Dodaj walidację na stworzonych wcześniej polach:
- text - jest wymagany
- date - data musi być późniejsza niż dzień dzisiejszy i jest wymagana

Wyświetl błędy po kliknięciu na button pod odpowiednimi polami. Po poprawnym dodaniu nowego elementu wartości pól powinny być resetowane.

**Zadanie 3.** (0.5 pkt) Dodaj listę to-do poniżej formularza. Element to-do powinien być osobnym komponentem. Dodaj możliwość usunięcia wybranego elementu lub oznaczenia go jako ukończony. Ukończonych zadań nie powinno dać się usunąć. Pojedyncze zadanie powinno się wyświetlać w formacie:

"lp." "Text" "Data w formacie dd/MM/yyyy" "Przycisk usuń" "Przycisk zakończ zadanie"

Po naciśnięciu na przycisk zakończ zadanie, dany element niech ma zielone tło.

**Zadanie 4.** (0.3 pkt) Ponownie stwórz projekt za pomocą polecenia npx create-react-app. Za pomocą biblioteki axios pobierz rekordy z API dostępnego pod linkiem: https://restcountries.eu/rest/v2. API udostępnia listę krajów. 

Następnie wyświetl pobrane rekordy w postaci kafelek umieszczonych w trzech kolumnach. Pojedynczy kafelek powinien być osobnym komponentem. Pojedynczy element powinien zawierać zdjęcie flagi państwa, a pod flagą powinna znaleźć się jego nazwa. 

Podgląd:  
![countries](Countries_sketch.png)

**Zadanie 5.** (0.4 pkt) Powyżej listy dodaj formularz z polem tekstowym oraz przyciskiem. Po wpisaniu nazwy kraju (bądź jej części) i zatwierdzeniu przyciskiem lub enterem na liście powinny zostać kraje, których nazwa odpowiada tekstowi wpisanemu w polu. 

Następnie dodaj przycisk sortowania - po kliknięciu niech lista krajów będzie sortowana alfabetycznie wg. nazwy krajów rosnąco. Po ponownym kliknięciu na ten przycisk kolejność odwraca się. Sortowanie i filtrowanie powinno działać równocześnie.  

**Zadanie 6.** (0.3 pkt) Dodaj nowy filtr - po wpisaniu skrótu nazwy kraju ("ITA", "SVN" itd.) wyświetli wszystkie kraje, które graniczą z tym krajem. 